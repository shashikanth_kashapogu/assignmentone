/**
 * 
 */
package com.springboot.webapp.asgnmnt3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springboot.webapp.asgnmnt3.exception.RecordNotFoundException;
import com.springboot.webapp.asgnmnt3.model.EmployeeModel;
import com.springboot.webapp.asgnmnt3.service.EmployeeService;

/**
 * @author shash
 *
 */
@Controller
@RequestMapping("/api/employees")
public class EmployeeController
{
    @Autowired
    EmployeeService service;
 
    @GetMapping
	public String getAllEmployees(Model model) {
    	List<EmployeeModel> list = service.getAllEmployees();
		model.addAttribute("employees", list);
		return "listEmployees";
	}
    
    @GetMapping("/edit")
	public String addEmployee(Model model) {

    	return "addEmployee";
	}
    
	@RequestMapping(value = "/createEmployee", method = RequestMethod.POST)
	@ResponseBody
    public String addEmployeeAction(@ModelAttribute("employee") EmployeeModel employee, Model model) throws RecordNotFoundException {
    	EmployeeModel updated = service.createOrUpdateEmployee(employee);
		model.addAttribute("employee", updated);
		return "Record has been successfully saved.";
	}
    
}