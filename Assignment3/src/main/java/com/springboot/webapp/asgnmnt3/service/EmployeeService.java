/**
 * 
 */
package com.springboot.webapp.asgnmnt3.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.webapp.asgnmnt3.exception.RecordNotFoundException;
import com.springboot.webapp.asgnmnt3.model.EmployeeModel;
import com.springboot.webapp.asgnmnt3.repository.EmployeeRepository;

/**
 * @author shash
 *
 */
@Service
public class EmployeeService {
     
    @Autowired
    EmployeeRepository repository;
     
    public List<EmployeeModel> getAllEmployees()
    {
        List<EmployeeModel> employeeList = repository.findAll();
         
        if(employeeList.size() > 0) {
            return employeeList;
        } else {
            return new ArrayList<EmployeeModel>();
        }
    }
     
    public EmployeeModel getEmployeeById(Long id) throws RecordNotFoundException
    {
        Optional<EmployeeModel> employee = repository.findById(id);
         
        if(employee.isPresent()) {
            return employee.get();
        } else {
            throw new RecordNotFoundException("No employee record exist for given id");
        }
    }
     
    public EmployeeModel createOrUpdateEmployee(EmployeeModel entity) throws RecordNotFoundException
    {
        //Optional<EmployeeModel> employee = repository.findById(entity.getId());
         
        entity = repository.save(entity);
        return entity;
/*        if(employee.isPresent())
        {
        	EmployeeModel newEntity = employee.get();
            newEntity.setMiddleName(entity.getMiddleName());
            newEntity.setFirstName(entity.getFirstName());
            newEntity.setLastName(entity.getLastName());
 
            newEntity = repository.save(newEntity);
             
            return newEntity;
        } else {
            entity = repository.save(entity);
             
            return entity;
        }*/
    }
     
    public void deleteEmployeeById(Long id) throws RecordNotFoundException
    {
        Optional<EmployeeModel> employee = repository.findById(id);
         
        if(employee.isPresent())
        {
            repository.deleteById(id);
        } else {
            throw new RecordNotFoundException("No employee record exist for given id");
        }
    }
}