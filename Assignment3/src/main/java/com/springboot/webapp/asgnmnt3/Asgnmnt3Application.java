package com.springboot.webapp.asgnmnt3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Asgnmnt3Application {

	public static void main(String[] args) {
		SpringApplication.run(Asgnmnt3Application.class, args);
	}

}
