/**
 * 
 */
package com.springboot.webapp.asgnmnt3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboot.webapp.asgnmnt3.model.DepartmentModel;

/**
 * @author shash
 *
 */
@Repository
public interface DepartmentRepository extends JpaRepository<DepartmentModel, Long> {
	 
}
