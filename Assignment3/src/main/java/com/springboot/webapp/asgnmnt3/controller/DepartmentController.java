/**
 * 
 */
package com.springboot.webapp.asgnmnt3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springboot.webapp.asgnmnt3.exception.RecordNotFoundException;
import com.springboot.webapp.asgnmnt3.model.DepartmentModel;
import com.springboot.webapp.asgnmnt3.service.DepartmentService;

/**
 * @author shash
 *
 */
@Controller
@RequestMapping("/api/departments")
public class DepartmentController
{
    @Autowired
    DepartmentService departmentService;
 
    @GetMapping
	public String getAllEmployees(Model model) {
    	List<DepartmentModel> list = departmentService.getAllDepartments();
		model.addAttribute("departments", list);
		return "listDepartments";
	}
    
    @GetMapping("/add")
	public String addDepartment(Model model) {

    	return "addDepartment";
	}
    
	@RequestMapping(value = "/createDepartment", method = RequestMethod.POST)
	@ResponseBody
    public String addDepartmentAction(@ModelAttribute("department") DepartmentModel department, Model model) throws RecordNotFoundException {
		DepartmentModel updated = departmentService.createOrUpdateDepartment(department);
		model.addAttribute("employee", updated);
		return "Record has been successfully saved.";
	}
    
/*    @GetMapping("/getAll")
    public ResponseEntity<List<DepartmentModel>> getAllDepartments() {
        List<DepartmentModel> list = departmentService.getAllDepartments();
 
        return new ResponseEntity<List<DepartmentModel>>(list, new HttpHeaders(), HttpStatus.OK);
    }*/
 
/*    @GetMapping("/{id}")
    public ResponseEntity<DepartmentModel> getDepartmentById(@PathVariable("id") Long id)
                                                    throws RecordNotFoundException {
    	DepartmentModel department = departmentService.getDepartmentById(id);
 
        return new ResponseEntity<DepartmentModel>(department, new HttpHeaders(), HttpStatus.OK);
    }*/
 
/*    @PostMapping("/addoreditdepartment")
    public ResponseEntity<DepartmentModel> createOrUpdateDepartment(@RequestBody DepartmentModel department)
                                                    throws RecordNotFoundException {
    	DepartmentModel updated = departmentService.createOrUpdateDepartment(department);
        return new ResponseEntity<DepartmentModel>(updated, new HttpHeaders(), HttpStatus.OK);
    }
 
    @DeleteMapping("/{id}")
    public HttpStatus deleteDepartmentById(@PathVariable("id") Long id)
                                                    throws RecordNotFoundException {
    	departmentService.deleteEmployeeById(id);
        return HttpStatus.FORBIDDEN;
    }*/
 
}