/**
 * 
 */
package com.springboot.webapp.asgnmnt3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboot.webapp.asgnmnt3.model.EmployeeModel;

/**
 * @author shash
 *
 */
@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeModel, Long> {
 
}
