/**
 * 
 */
package com.springboot.webapp.asgnmnt3.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.webapp.asgnmnt3.exception.RecordNotFoundException;
import com.springboot.webapp.asgnmnt3.model.DepartmentModel;
import com.springboot.webapp.asgnmnt3.repository.DepartmentRepository;

/**
 * @author shash
 *
 */
@Service
public class DepartmentService {
     
    @Autowired
    DepartmentRepository departmentRepository;
     
    public List<DepartmentModel> getAllDepartments()
    {
        List<DepartmentModel> departmentList = departmentRepository.findAll();
         
        if(departmentList.size() > 0) {
            return departmentList;
        } else {
            return new ArrayList<DepartmentModel>();
        }
    }
     
    public DepartmentModel getDepartmentById(Long id) throws RecordNotFoundException
    {
        Optional<DepartmentModel> department = departmentRepository.findById(id);
         
        if(department.isPresent()) {
            return department.get();
        } else {
            throw new RecordNotFoundException("No department record exist for given id");
        }
    }
     
    public DepartmentModel createOrUpdateDepartment(DepartmentModel departmentM) throws RecordNotFoundException
    {
        Optional<DepartmentModel> department = departmentRepository.findById(departmentM.getId());
         
        if(department.isPresent())
        {
        	DepartmentModel newEntity = department.get();
            newEntity.setName(departmentM.getName());
            newEntity = departmentRepository.save(newEntity);
             
            return newEntity;
        } else {
        	departmentM = departmentRepository.save(departmentM);
             
            return departmentM;
        }
    }
     
    public void deleteEmployeeById(Long id) throws RecordNotFoundException
    {
        Optional<DepartmentModel> department = departmentRepository.findById(id);
         
        if(department.isPresent())
        {
        	departmentRepository.deleteById(id);
        } else {
            throw new RecordNotFoundException("No department record exist for given id");
        }
    }
}