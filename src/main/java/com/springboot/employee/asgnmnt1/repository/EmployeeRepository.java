/**
 * 
 */
package com.springboot.employee.asgnmnt1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboot.employee.asgnmnt1.model.EmployeeModel;

/**
 * @author shash
 *
 */
@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeModel, Long> {
 
}
