package com.springboot.employee.asgnmnt1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Asgnmnt1Application {

	public static void main(String[] args) {
		SpringApplication.run(Asgnmnt1Application.class, args);
	}

}
