import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
declare const $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  baseURL = "http://localhost:8090/api";
  isNewRecord = true;
  record = { id: "", firstName: "", middleName: "", lastName: "" };
  recordList: any = [];

  constructor(private httpClient: HttpClient) {
    this.loadRecords();
  }

  openRecordDialog(isNewRecord: any, record?: any) {
    this.isNewRecord = isNewRecord;
    this.record = this.isNewRecord ? { id: "", firstName: "", middleName: "", lastName: "" } : record;
    $('#recordModel').modal('show');
  }

  addUpdateRecord() {
    this.httpClient.post(`${this.baseURL}/addoreditemployee`, this.record).subscribe(
      (res: any) => {
        this.loadRecords();
      },
      (err: any) => { console.log(err); }
    )
    $('#recordModel').modal('hide');
  }

  loadRecords() {
    this.httpClient.get(`${this.baseURL}/employees`).subscribe((res: any) => {
      this.recordList = res;
    }, (err: any) => { console.log(err); })
  }

  deleteRecordConfirmation(record: any) {
    this.record = record;
    $("#deleteConfirmation").modal('show');
  }

  deleteRecord() {
    this.httpClient.delete(`${this.baseURL}/${this.record.id}`).subscribe((res: any) => {
      this.loadRecords();
    }, (err: any) => { console.log(err); })
    $("#deleteConfirmation").modal('hide');
  }

}
